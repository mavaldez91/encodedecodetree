import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class DecodeStringMain {

    /**
     * Reads a file
     * @param filename is file to read
     * @return list of strings. Lines of a file.
     */
    public static List<String> readFile(String filename){
        InputStream in = DecodeStringMain.class.getResourceAsStream(filename);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        List<String> rtn = new ArrayList<>();

        try {
            for(String line = reader.readLine(); line != null; line = reader.readLine()){
                rtn.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return rtn;
        }
        return rtn;
    }

    /**
     * Creates a decode Tree
     * @param decodeTree DecodeTree to create
     * @param words words to create the decode tree
     */
    public static void populateTree(DecodeTree decodeTree, List<String> words){
        for(String word : words){
            String[] splitStr = word.split(" ");
            decodeTree.insert(splitStr[WORD_INDEX], splitStr[ENCODE_INDEX]);
        }
    }

    /**
     * Using the DecodeTree. Decode encoded sentences
     * @param encodedSentences is a list of sentences to decode
     * @param decodeTree is the tree to use to decode the encoded sentences
     */
    public static void decodeSentences(List<String> encodedSentences, DecodeTree decodeTree){
        for(String sentence : encodedSentences){
            String[] words = sentence.split(" ");
            StringBuilder sb = new StringBuilder();
            for(String word : words){
                sb.append(decodeTree.search(word));
                sb.append(" ");
            }

            System.out.println(sb.toString());
        }
    }



    public static void main(String[] args) {
        List<String> words = DecodeStringMain.readFile("words.txt");
        List<String> encodedSentences = DecodeStringMain.readFile("encoded.txt");

        DecodeTree decodeTree = new DecodeTree();
        DecodeStringMain.populateTree(decodeTree, words);

        DecodeStringMain.decodeSentences(encodedSentences, decodeTree);


    }

    public static final int WORD_INDEX = 0;
    public static final int ENCODE_INDEX = 1;

}
