public class DecodeNode {
    String word;
    DecodeNode left;
    DecodeNode right;

    public static DecodeNode newNode(String word){
        DecodeNode rtn = new DecodeNode();
        rtn.word = word;
        rtn.left = null;
        rtn.right = null;
        return rtn;
    }

}
