/**
 * A DecodeTree to aid in decoding encoded "+-" Stings
 */

public class DecodeTree {
    DecodeNode rootNode;

    public DecodeTree(){
        this.rootNode = DecodeNode.newNode("");
    }

    /**
     * Insert Word into Tree. Calls recursive insert starting at root Node
     * @param word to be inserted
     * @param encodeString  of word
     */
    public void insert(String word, String encodeString){
        insert(rootNode, word, encodeString);
    }

    /**
     * Recursive Insert Function
     * @param node to search for insert
     * @param word to insert
     * @param encodeString of word
     */
    private void insert(DecodeNode node, String word, String encodeString) {
        String[] encodeStr = encodeString.split("");

        if(encodeStr.length <= 0){
            return;
        }

        switch (encodeStr[0]) {
            case "+":
                if (encodeStr.length == 1)
                    node.right = DecodeNode.newNode(word);
                else if (node.right == null) {
                    node.right = DecodeNode.newNode("");
                    insert(node.right, word, encodeString.substring(1));
                } else
                    insert(node.right, word, encodeString.substring(1));

                break;
            case "-":
                if (encodeStr.length == 1)
                    node.left = DecodeNode.newNode(word);
                else if (node.left == null) {
                    node.left = DecodeNode.newNode("");
                    insert(node.left, word, encodeString.substring(1));
                } else
                    insert(node.left, word, encodeString.substring(1));
                break;
        }
    }

    /**
     * Calls recursive search starting at root node to decode word
     * @param encodeString decode this string
     * @return decoded string
     */
    public String search(String encodeString){
        return search(this.rootNode, encodeString);
    }

    /**
     * Recursive search to find encoded string in tree
     * @param node to search for encoded string
     * @param encodeString of word
     * @return decoded word from encodeString
     */
    private String search(DecodeNode node, String encodeString){
        String[] encodeStr = encodeString.split("");
        String rtn = "";

        if(encodeStr.length <= 0){
            return rtn;
        }

        switch (encodeStr[0]) {
            case "+":
                if (encodeStr.length == 1)
                    return node.right.word;
                else
                    rtn = search(node.right, encodeString.substring(1));
                break;
            case "-":
                if (encodeStr.length == 1)
                    return node.left.word;
                else
                    rtn = search(node.left, encodeString.substring(1));
                break;
        }

        return rtn;
    }

}
